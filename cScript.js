function hexToR(h) {
	return parseInt((cutHex(h)).substring(0,2),16);
}
function hexToG(h) {
	return parseInt((cutHex(h)).substring(2,4),16);
}
function hexToB(h) {
	return parseInt((cutHex(h)).substring(4,6),16);
}
function cutHex(h) {return (h.charAt(0)=="#") ? h.substring(1,7):h;}
function toHex(c){
	var n = parseInt(c,10);
	if (isNaN(n)) return "00";
	n = Math.max(0,Math.min(n,255));
	return "0123456789ABCDEF".charAt((n-n%16)/16)
      + "0123456789ABCDEF".charAt(n%16);
}
function rgbToHex(r,g,b){
	return "#" + toHex(r) + toHex(g) + toHex(b);
}
function parseData(){
	var data = document.getElementById("data").innerHTML;
	data = data.split('\n');
	var i = 0;
	for(i = 0; i < data.length; i++){
		data[i] = data[i].split(',');
	}
	return data;
}
function updateSquare(e){
	var r = rInput.value;
	var g = gInput.value;
	var b = bInput.value;
	var s = document.getElementById("cSquare");
	var h = rgbToHex(r,g,b);
	s.style.backgroundColor = h;
	r = hexToR(h);
	g = hexToG(h);
	b = hexToB(h);
}
function distance(vec1,vec2){
	var x = Math.pow(vec2[0] - vec1[0],2);
	var y = Math.pow(vec2[1] - vec1[1],2);
	var z = Math.pow(vec2[2] - vec1[2],2);
	return Math.sqrt(x + y + z);
}
function findNearest(r,g,b){
	var closest = ["",Infinity];
	var i;
	for(i = 0; i < data.length; i++){
		var h = data[i][1];
		var cR = hexToR(h);
		var cG = hexToG(h);
		var cB = hexToB(h);
		var d = distance([r,b,g],[cR,cG,cB]);
		if(d < closest[1]){
			closest = [data[i][0],d];
			if(d == 0){
				break;
			}
		}
	}
	return closest;
}
function find2Nearest(r,g,b){
	var firstNearest = findNearest(r,g,b);
	var secondNearest = ["",Infinity];
	if(firstNearest[1] == 0){
		//perfect match
		secondNearest = firstNearest;
		return [firstNearest[0],secondNearest[0]];
	}
	var i;
	for(i = 0; i < data.length; i++){
		var h = data[i][1];
		var cR = hexToR(h);
		var cG = hexToG(h);
		var cB = hexToB(h);
		var d = distance([r,b,g],[cR,cG,cB]);
		if(d < secondNearest[1]){
			if(data[i][0] != firstNearest[0]){
				secondNearest = [data[i][0],d];
			}
		}
	}
	return [firstNearest[0],secondNearest[0]];
}
function mergeStrings(s1,s2){
	var fLen = Math.round((s1.length + s2.length)/2);
	var fString = "";
	for(var i = 0; i < fLen; i++){
		var c;
		if( i < s1.length && i < s2.length){
			var r = Math.round(Math.random());
			if(r == 1){
				c = s1[i];
			}
			else{
				c = s2[i];
			}
		}
		else if(i < s1.length){
			c = s1[i];
		}
		else if(i < s2.length){
			c = s2[i];
		}
		fString += c;
	}
	return fString;
}
function colorize(){
	var r = rInput.value;
	var g = gInput.value;
	var b = bInput.value;
	var n = find2Nearest(r,g,b);
	var s = mergeStrings(n[0],n[1]);
	output.innerHTML = s;
}
const data = parseData();
const rInput = document.getElementById("rValue");
const gInput = document.getElementById("gValue");
const bInput = document.getElementById("bValue");
const output = document.getElementById("cName");
rInput.addEventListener("input",updateSquare);
gInput.addEventListener("input",updateSquare);
bInput.addEventListener("input",updateSquare);
updateSquare();